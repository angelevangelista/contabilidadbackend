﻿using ContabilidadBackEnd.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ContabilidadBackEnd.ViewModels;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ContabilidadBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrigenesCuentasAsientosController : ControllerBase
    {
        private dbContabilidad _context;

        public OrigenesCuentasAsientosController(dbContabilidad context)
        {
            _context = context;

        }


        // GET api/<OrigenesCuentasAsientos>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var consult = _context.TipoAsientosCuentas.Include("CuentasContables").Where(x => x.TipoAsientoID == id).ToList();

            List<AsientoCuenta> CuentasAsientos = new List<AsientoCuenta>();

            foreach (var item in consult)
            {
                CuentasAsientos.Add(new AsientoCuenta { NombreCuenta = item.CuentasContables.CuentaNombre, TipoAsientosCuentasID = item.TipoAsientosCuentasID });
            }

            var result= new VmAsientosCuentasOrigen { AsientoCuentas= CuentasAsientos };



            return Ok(result);
        }

        // POST api/<OrigenesCuentasAsientos>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<OrigenesCuentasAsientos>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OrigenesCuentasAsientos>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
