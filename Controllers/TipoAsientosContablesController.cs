﻿using ContabilidadBackEnd.Models;
using ContabilidadBackEnd.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;



namespace ContabilidadBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoAsientosContablesController : ControllerBase
    {
        private dbContabilidad _context;

        public TipoAsientosContablesController(dbContabilidad context)
        {
            _context = context;

        }


        // GET: api/<TipoAsientosContables>
        [HttpGet]
        public IActionResult Get()
        {
            var Consulta = _context.TipoAsientosContables.Include("TipoAsientosCuentas").Select(x => new
            {
                TipoAsientoID = x.TipoAsientoID,
                TipoAsientoNombre = x.TipoAsientoNombre,
                NoCuentas = x.TipoAsientosCuentas.Count(),
                Estado = x.TipoAsientoEstado

            }).ToList();

            return Ok(Consulta);
        }


        // GET api/<TipoAsientosContables>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {

            var Consulta = _context.TipoAsientosContables.Include("TipoAsientosCuentas").Where(x => x.TipoAsientoID == id).Select(x => new
            {
                TipoAsientoID = x.TipoAsientoID,
                TipoAsientoNombre = x.TipoAsientoNombre,
                CuentasContables = x.TipoAsientosCuentas.Select(x => x.CuentasContablesID),
                Estado = x.TipoAsientoEstado

            });

            return Ok(Consulta);
        }

        // POST api/<TipoAsientosContables>
        [HttpPost]
        public void Post(VmAsientosCuentas CuentasAsientos)
        {

            TipoAsientosContables TipoAsiento = new TipoAsientosContables
            {
                TipoAsientoNombre = CuentasAsientos.NombreAsiento,
                TipoAsientoEstado = true
            };
            _context.TipoAsientosContables.Add(TipoAsiento);
            _context.SaveChanges();

            List<TipoAsientosCuentas> ListaCuentas = new List<TipoAsientosCuentas>();


            foreach (var Cuenta in CuentasAsientos.CuentasContables)
            {
                ListaCuentas.Add(new TipoAsientosCuentas { CuentasContablesID = Cuenta, TipoAsientoID = TipoAsiento.TipoAsientoID });
            }

            _context.AddRange(ListaCuentas);
            _context.SaveChanges();

        }

        // PUT api/<TipoAsientosContables>/5
        [HttpPut("{id}")]
        public void Put(int id, VmAsientosCuentas CuentasAsientos)
        {
            var Asiento = _context.TipoAsientosContables.Include("TipoAsientosCuentas").FirstOrDefault(x => x.TipoAsientoID == id);

            Asiento.TipoAsientoEstado = CuentasAsientos.Estado;

            if (Asiento.TipoAsientoNombre != CuentasAsientos.NombreAsiento)
            {
                Asiento.TipoAsientoNombre = CuentasAsientos.NombreAsiento;
            }

            _context.SaveChanges();

            if (CuentasAsientos.CuentasContables !=null)
            {
                if(CuentasAsientos.CuentasContables[0] == -2)
                {
                    _context.RemoveRange(Asiento.TipoAsientosCuentas);
                    _context.SaveChanges();
                }
                else
                {
                    _context.RemoveRange(Asiento.TipoAsientosCuentas);
                    _context.SaveChanges();

                    List<TipoAsientosCuentas> ListaCuentas = new List<TipoAsientosCuentas>();

                    foreach (var Cuenta in CuentasAsientos.CuentasContables)
                    {
                        ListaCuentas.Add(new TipoAsientosCuentas { CuentasContablesID = Cuenta, TipoAsientoID = Asiento.TipoAsientoID });
                    }

                    _context.AddRange(ListaCuentas);
                    _context.SaveChanges();
                }
            }
           
        }

        // DELETE api/<TipoAsientosContables>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
