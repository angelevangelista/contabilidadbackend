﻿using ContabilidadBackEnd.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ContabilidadBackEnd.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UtilitiesController : ControllerBase
    {
        private dbContabilidad _context;

        public UtilitiesController(dbContabilidad context)
        {
            _context = context;

        }

        [HttpGet]
        public IEnumerable<CuentaOrigen> CuentaOrigen()
        {
           return  _context.CuentaOrigen.ToList();

        }


        [HttpGet]
        public IEnumerable<CuentaTipo> CuentaTipo()
        {
            return _context.CuentaTipo.ToList();

        }

        [HttpGet]
        public IEnumerable<CuentaJerarquia> CuentaJerarquia()
        {
            return _context.CuentaJerarquia.ToList();

        }

        [HttpGet]
        public IActionResult CuentasContables()
        {
           
            var cuentas= _context.CuentasContables.Where(x => x.CuentaEstado == true).Select(x => new
            {
                CuentasContablesID = x.CuentasContablesID,
                CuentaNombre = x.CuentaNombre
            }).ToList();

            return Ok(cuentas);
        }

        [HttpGet]
        public bool IndexCuentas()
        {
          /* var Cuentas= _context.CuentasContables.Include("CuentaJerarquia").Where(x => x.CuentaJerarquia.CuentaJerarquiaNombre == "Madre").ToList();

            foreach (var item in Cuentas)
            {
                var CuentasHijas= _context.CuentasContables.Where(x=> x.Nom)
            }*/

            return true;
        }

    }
}
