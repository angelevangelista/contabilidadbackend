﻿using ContabilidadBackEnd.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using ContabilidadBackEnd.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace ContabilidadBackEnd.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class CuentasContablesController : ControllerBase
    {
        private dbContabilidad _context;

        public CuentasContablesController(dbContabilidad context)
        {
            _context = context;

        }


        // GET: api/<CuentasContables>
        [HttpGet]
        public List<VmCuentasContables> Get()
        {
            var Consulta = _context.CuentasContables.Select(x => new VmCuentasContables
            { 
                CuentasContablesID= x.CuentasContablesID, 
                CuentaNombre = x.CuentaNombre, 
                CuentaNomenclatura = x.CuentaNomenclatura, 
                CuentaEstado = x.CuentaEstado 
            }).ToList();

            return Consulta;
        }


        // GET api/<CuentasContables>/5
        [HttpGet("{id}")]
        public VmCuentasContables Get(int id)
        {
            var consulta = _context.CuentasContables.
                Include("CuentaOrigen").
                Include("CuentaJerarquia").
                Include("CuentaTipo").
                FirstOrDefault(x => x.CuentasContablesID == id);

            var Edit = new VmCuentasContables { 
                
                CuentasContablesID = consulta.CuentasContablesID,
                CuentaEstado = consulta.CuentaEstado,
                CuentaJerarquia = consulta.CuentaJerarquia.CuentaJerarquiaNombre,
                CuentaTipo = consulta.CuentaTipo.CuentaTipoNombre,
                CuentaOrigen = consulta.CuentaOrigen.CuentaOrigenNombre,
                CuentaNomenclatura = consulta.CuentaNomenclatura,
                CuentaNombre = consulta.CuentaNombre,
                CuentaRelaciones= consulta.CuentaRelaciones
            
            };

            return Edit;
        }

        // POST api/<CuentasContables>
        [HttpPost]
        public IActionResult Post([FromBody] CuentasContables cuentas)
        {
            try
            {
                bool existenomenclatura = _context.CuentasContables.Any(x => x.CuentaNomenclatura == cuentas.CuentaNomenclatura);

                if (!existenomenclatura)
                {
                    _context.CuentasContables.Add(cuentas);
                    _context.SaveChanges();
                }
                else
                {
                    return new StatusCodeResult(400);
                }
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(406);
            }

            return new StatusCodeResult(200);

        }

        // PUT api/<CuentasContables>/5
        [HttpPut]
        public IActionResult Put([FromBody] VmCuentasContables cuenta)
        {

            try
            {
                bool existenomenclatura = _context.CuentasContables.Any(x => x.CuentaNomenclatura == cuenta.CuentaNomenclatura && x.CuentasContablesID != cuenta.CuentasContablesID);

                if (!existenomenclatura)
                {
                    var current = _context.CuentasContables.Find(cuenta.CuentasContablesID);

                    if (current.CuentaNomenclatura != cuenta.CuentaNomenclatura)
                        current.CuentaNomenclatura = cuenta.CuentaNomenclatura;

                    if (current.CuentaEstado != cuenta.CuentaEstado)
                        current.CuentaEstado = cuenta.CuentaEstado;

                    if (current.CuentaNombre != cuenta.CuentaNombre)
                        current.CuentaNombre = cuenta.CuentaNombre;
                    
                    if(current.CuentaRelaciones != cuenta.CuentaRelaciones)
                        current.CuentaRelaciones = cuenta.CuentaRelaciones;



                    _context.SaveChanges();

                }
                else
                {
                    return new StatusCodeResult(400);

                }

            }
            catch (Exception p)
            {

                return new StatusCodeResult(406);
            }

            return new StatusCodeResult(200);

        }

        // DELETE api/<CuentasContables>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var update = _context.CuentasContables.Find(id);
            update.CuentaEstado = false;
            _context.SaveChanges();

        }
    }
}
