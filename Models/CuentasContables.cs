﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class CuentasContables
    {
        [Key]
        public int CuentasContablesID { get; set; }
        public string CuentaNombre { get; set; }
        public string CuentaNomenclatura { get; set; }
        public bool CuentaEstado { get; set; } = true;

        [ForeignKey("CuentaOrigen")]
        public int CuentaOrigenID { get; set; }
        
        
        [ForeignKey("CuentaTipo")]
        public int CuentaTipoID { get; set; }
        
        
        [ForeignKey("CuentaJerarquia")]
        public int CuentaJerarquiaID { get; set; }

        public string CuentaRelaciones { get; set; }


        #region Referencias a tablas

        public CuentaJerarquia CuentaJerarquia { get; set; }
        public CuentaOrigen CuentaOrigen { get; set; }
        public CuentaTipo CuentaTipo { get; set; }

        #endregion
    }
}
