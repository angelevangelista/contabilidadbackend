﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class TipoAsientosCuentas
    {
        public int TipoAsientosCuentasID { get; set; }

        [ForeignKey("TipoAsientosContables")]

        public int TipoAsientoID { get; set; }

        [ForeignKey("CuentasContables")]

        public int CuentasContablesID { get; set; }

        [ForeignKey("CuentaOrigen")]

        public int? CuentaOrigenID { get; set; }



        #region Referencias a tablas

        public CuentasContables CuentasContables { get; set; }
        public TipoAsientosContables TipoAsientosContables { get; set; }
        public CuentaOrigen CuentaOrigen { get; set; }

        #endregion
    }
}
