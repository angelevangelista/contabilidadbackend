﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;



namespace ContabilidadBackEnd.Models
{
    public class dbContabilidad : DbContext
    {
        #region Configurations 

        protected readonly IConfiguration Configuration;

        public dbContabilidad(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("DbContext"));
        }

        #endregion

        public virtual DbSet<CuentasContables> CuentasContables { get; set; }
        public virtual DbSet<CuentaJerarquia> CuentaJerarquia { get; set; }
        public virtual DbSet<CuentaOrigen> CuentaOrigen { get; set; }
        public virtual DbSet<CuentaTipo>  CuentaTipo {get;set;}
        public virtual DbSet<TipoAsientosContables> TipoAsientosContables { get; set; }
        public virtual DbSet<TipoAsientosCuentas> TipoAsientosCuentas { get; set; }
    }
}
