﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class CuentaJerarquia
    {
        [Key]
        public int CuentaJerarquiaID { get; set; }
        public string CuentaJerarquiaNombre { get; set; }
    }
}
