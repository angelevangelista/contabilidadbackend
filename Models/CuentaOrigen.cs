﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class CuentaOrigen
    {
        [Key]
        public int CuentaOrigenID { get; set; }
        public string CuentaOrigenNombre {get;set;}
    }
}
