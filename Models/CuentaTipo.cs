﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class CuentaTipo
    {
        [Key]
        public int CuentaTipoID { get; set; }
        public string CuentaTipoNombre { get; set; }
    }
}
