﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.Models
{
    public class TipoAsientosContables
    {
        [Key]
        public int TipoAsientoID { get; set; }
        public string TipoAsientoNombre { get; set; }
        public bool TipoAsientoEstado { get; set; }


        public ICollection<TipoAsientosCuentas> TipoAsientosCuentas { get; set; }

    }
}
