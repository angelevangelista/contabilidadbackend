﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.ViewModels
{
    public class VmAsientosCuentas
    {
        public string NombreAsiento { get; set; }
        public int[] CuentasContables { get; set; }

        public bool Estado { get; set; }
    }
}
