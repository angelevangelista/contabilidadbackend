﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.ViewModels
{
    public class VmCuentasContables
    {
        public int CuentasContablesID { get; set; }
        public string CuentaNombre { get; set; }
        public string CuentaNomenclatura { get; set; }
        public bool CuentaEstado { get; set; }
        public string CuentaJerarquia { get; set; }
        public string CuentaOrigen { get; set; }
        public string CuentaTipo { get; set; }
        public string CuentaRelaciones { get; set; }

    }
}
