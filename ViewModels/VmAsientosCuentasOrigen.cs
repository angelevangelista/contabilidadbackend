﻿using ContabilidadBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContabilidadBackEnd.ViewModels
{
    public class VmAsientosCuentasOrigen
    {
        public int TipoAsientoID { get; set; }
        public List<AsientoCuenta> AsientoCuentas { get; set; }
    }

    public class AsientoCuenta
    {
        public string NombreCuenta { get; set; }
        public int TipoAsientosCuentasID { get; set; }
        public int CuentaOrigenID { get; set; }

    }
}
